import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import './App.css'
import ListeJeux from './components/ListeJeux';
import UnSeulJeux from './components/UnSeulJeux';

function App() {

  let jeux =
  {
    title: "Hunt for games",
    description: "Avec Hunt for games, trouvez la perle rare au milieu d'un océan de jeux vidéo !"
  };

  // console.log(jeux);

  return (
    <div className="App">

      <>
        <Container className="p-3">
          <Container className="p-5 mb-4 bg-light rounded-3">
            <h1 className='TitreDeApplication'>{jeux.title}</h1>

            <p className='DescriptionApplication'>{jeux.description}</p>
          </Container>
        </Container>
      </>

      <>
        <UnSeulJeux />
      </>

      <>
        <ListeJeux />
      </>

    </div>
  );
};

export default App
