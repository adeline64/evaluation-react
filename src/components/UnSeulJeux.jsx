import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import React, { useEffect, useState } from "react";
import axios from "axios";

export default function UnSeulJeux() {

  const API_URL = "https://raw.githubusercontent.com/bastienapp/gamesapi/main/db.json";

  console.log(API_URL);

  const [game, setGame] = useState([]);

  const [favoris, setFavoris] = useState([]);


  let axiosAPI = useEffect(() => {
    axios
      .get(API_URL)

      .then(response => {
        console.log(response);
        setGame(response.data[0]);
      })

      .catch(error => {
        console.log(error);
      })

  }, []);


  return (
    <div id="UnSeulJeux" className="UnSeulJeux">

      <Container className="p-3 containerListeJeux">
        <Container className="p-5 mb-4 bg-light rounded-3">

          <p>
            <img src={game.background_image} alt="image du jeux" className='ImageSeul'/>
          </p>

          <h4 className='TitreJeux'>
            {game.name}
          </h4>

          <p className='date'>
            {game.released}
          </p>
          
        </Container>
      </Container>

    </div>
  )
}