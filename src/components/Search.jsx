import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function Search() {

    const Search = () => {

    }

    return (

        <div id="Search" className="Search">

            <Container className="p-3">
                <Container className="p-5 mb-4 bg-light rounded-3 containerSearch">

                    <form action="/" method="get" className='formSearch'>
                        <label htmlFor="header-search">
                            <span className="visually-hidden">Recherche</span>
                        </label>
                        <input
                            type="text"
                            id="header-search"
                            placeholder="Le titre"
                            name="s"
                        />
                        <Button type="submit" className='BtnPrimary' variant="primary">Valider</Button>{' '}
                    </form>
                </Container>
            </Container>
        </div>



    );
}