import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import axios from "axios";
import React, { useEffect, useState } from "react";
import Search from "./Search";


export default function ListeJeux() {

  const API_URL = "https://raw.githubusercontent.com/bastienapp/gamesapi/main/db.json";

  // console.log(API_URL);

  const [games, setGames] = useState([]);


  let axiosAPI = useEffect(() => {
    axios
      .get(API_URL)

      .then(response => {
        console.log(response);
        let occurrence = setGames(response.data);
        // console.log(occurrence);
      })

      .catch(error => {
        console.log(error);
      })

  }, []);

  // console.log(axiosAPI);

  return (
    <div id="ListeJeux" className="ListeJeux">

      <>

        <Search />

      </>

      <>
        <Container className="p-3 containerListeJeux">
          <Container className="p-5 mb-4 bg-light rounded-3 placeImageEtTitre">


            {games.map(occurrence => (
              <div key={occurrence.id}>

                <h2 className="TitreListeJeux">
                  {occurrence.name}
                </h2>

                <br />

                <p>
                  <img src={occurrence.background_image} alt="image du jeux" className="ImageListe" />
                </p>

              </div>
            ))}
          </Container>
        </Container>
      </>
    </div>
  );
}